/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQLConn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author arashdeep
 */
public class conn {

    Connection conn;
    String json;
    Connection connection = (Connection) conn();
    Statement stmt;

    public Statement CreateStatement() {
        try {
            stmt = connection.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(conn.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stmt;
    }

    public Connection conn() {
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(".ini"))) {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                json = sb.toString();
            }
        } catch (IOException ioe) {

        }
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject newJSON = jsonObject.getJSONObject("db");
            jsonObject = new JSONObject(newJSON.toString());
            String host = jsonObject.getString("host");
            String user = jsonObject.getString("user");
            String pass = jsonObject.getString("pass");
            String port = jsonObject.getString("port");
            String name = jsonObject.getString("name");

            try {
                conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + name + "", user, pass);
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                Platform.exit();
            }
        } catch (JSONException jsone) {

        }
        return conn;
    }

}
