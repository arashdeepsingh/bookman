package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    AnchorPane ap;
    @FXML
    AnchorPane mainap;
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        ap.getChildren().clear();
        ap.getChildren().add((Node) FXMLLoader.load(getClass().getResource("AddBook.fxml")));
        //        FXMLLoader Loader = new FXMLLoader();
        //        Loader.setLocation(getClass().getResource("AddBook.fxml"));
        //        try {
        //            Loader.load();
        //        } catch (LoadException ex) {
        //            System.out.println(ex);
        //        } catch (IOException ex) {
        //        }
        //
        //        Parent logparent = Loader.getRoot();
        //        Scene scene = new Scene(logparent);
        //        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        //        app_stage.setTitle("Admin Here!");
        //        app_stage.setScene(scene);
        //        app_stage.show();
    }
    
    @FXML
    private void SellAction(ActionEvent event) throws IOException {
        ap.getChildren().clear();
        ap.getChildren().add((Node) FXMLLoader.load(getClass().getResource("SellBook.fxml")));
//        FXMLLoader Loader = new FXMLLoader();
//        Loader.setLocation(getClass().getResource("SellBook.fxml"));
//        try {
//            Loader.load();
//        } catch (LoadException ex) {
//        } catch (IOException ex) {
//        }
//        Parent logparent = Loader.getRoot();
//        Scene scene = new Scene(logparent);
//        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//        app_stage.setTitle("Sell!");
//        app_stage.setScene(scene);
//        app_stage.show();
    }
    
    @FXML
    private void CategoryAction(ActionEvent event) throws IOException {
        ap.getChildren().clear();
        ap.getChildren().add((Node) FXMLLoader.load(getClass().getResource("BookCetagory.fxml")));

//        FXMLLoader Loader = new FXMLLoader();
//        Loader.setLocation(getClass().getResource("BookCetagory.fxml"));
//        try {
//            Loader.load();
//        } catch (LoadException ex) {
//        } catch (IOException ex) {
//        }
//        Parent logparent = Loader.getRoot();
//        Scene scene = new Scene(logparent);
//        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//        app_stage.setTitle("Sell!");
//        app_stage.setScene(scene);
//        app_stage.show();
    }
    
    @FXML
    private void ListBookAction(ActionEvent event) throws IOException {
        ap.getChildren().clear();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("ListBook.fxml"));
        Loader.load();
        ap.getChildren().add((Node) Loader.getRoot());
        ListBookController tolist = Loader.getController();
        tolist.BookView();
//        FXMLLoader Loader = new FXMLLoader();
//        Loader.setLocation(getClass().getResource("ListBook.fxml"));
//        try {
//            Loader.load();
//        } catch (LoadException ex) {
//        } catch (IOException ex) {
//        }
//        Parent logparent = Loader.getRoot();
//        Scene scene = new Scene(logparent);
//        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//        app_stage.setTitle("Sell!");
//        app_stage.setScene(scene);
//        app_stage.show();
//        ListBookController ToList = Loader.getController();
//        ToList.BookView();
    }
    
    @FXML
    private void addcategoryAction(ActionEvent event) throws IOException {
        ap.getChildren().clear();
        ap.getChildren().add((Node) FXMLLoader.load(getClass().getResource("AddCetagory.fxml")));
//        FXMLLoader Loader = new FXMLLoader();
//        Loader.setLocation(getClass().getResource("AddCetagory.fxml"));
//        try {
//            Loader.load();
//        } catch (LoadException ex) {
//        } catch (IOException ex) {
//        }
//        Parent logparent = Loader.getRoot();
//        Scene scene = new Scene(logparent);
//        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//        app_stage.setTitle("Sell!");
//        app_stage.setScene(scene);
//        app_stage.show();
    }
    
    public void EditBook(String ISBN) {
        ap.getChildren().clear();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("UpdateBook.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ap.getChildren().add((Node) Loader.getRoot());
        UpdateBookController toupdate = Loader.getController();
        toupdate.setISBN(ISBN);
    }
    
    public void ListBookCetagory(int cetagory) {
        ap.getChildren().clear();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("ListBook.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ap.getChildren().add((Node) Loader.getRoot());
        ListBookController tolist = Loader.getController();
        tolist.BookView(cetagory);
    }
    
    public void Recept(String JSON) {
        ap.getChildren().clear();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("Recept.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ap.getChildren().add((Node) Loader.getRoot());
        ReceptController torecept = Loader.getController();
        torecept.getInfo(JSON);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
}
