package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.json.JSONException;
import org.json.JSONObject;

public class SellBookController implements Initializable {

    @FXML
    TextField ISBNCodeField, quantityField;
    @FXML
    Label namelabel, costLabel;
    String ISBNCode;
    conn connection = new conn();
    Statement stmt;
    int quantity;
    JSONObject BookInfoObj = new JSONObject();
    JSONObject jsonObj = new JSONObject();
    String JSON;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ISBNCodeField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    try {
                        ISBNCode = ISBNCodeField.getText();
                        stmt = connection.CreateStatement();
                        ResultSet rs = stmt.executeQuery("SELECT * FROM `books` WHERE code='" + ISBNCode + "'");
                        while (rs.next()) {
                            namelabel.setText(rs.getString("title"));
                            costLabel.setText(rs.getString("cost"));
                        }
                        URL url;
                        try {
                            if (checkISBNInDb(ISBNCode)) {
                                quantityField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                                    @Override
                                    public void handle(KeyEvent event) {
                                        if (event.getCode() == KeyCode.ENTER) {
                                            quantity = Integer.parseInt(quantityField.getText());
                                            FXMLLoader Loader = new FXMLLoader();
                                            Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                                            try {
                                                Loader.load();
                                            } catch (LoadException ex) {
                                            } catch (IOException ex) {
                                            }
                                            Parent logparent = Loader.getRoot();
                                            Scene scene = new Scene(logparent);
                                            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                            app_stage.setTitle("Recept!");
                                            app_stage.setScene(scene);
                                            app_stage.show();
                                            FXMLDocumentController torecept = Loader.getController();
                                            addMore();
                                            JSON = jsonObj.toString();
                                            torecept.Recept(JSON);
                                        }
                                    }
                                });
                            } else {
                                Alert alert = new Alert(Alert.AlertType.WARNING);
                                alert.setTitle("Book Not Found");
                                alert.setContentText("Book Not Found In DataBase Please Add Book");
                                alert.showAndWait();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(AddBookController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(SellBookController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    @FXML
    private void SubmitAction(ActionEvent event) {
        try {
            quantity = Integer.parseInt(quantityField.getText());
            if (checkQuantity(quantity, ISBNCode)) {

                jsonObj.append(ISBNCode, quantity);
                FXMLLoader Loader = new FXMLLoader();
                Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                try {
                    Loader.load();
                } catch (LoadException ex) {
                } catch (IOException ex) {
                }

                Parent logparent = Loader.getRoot();
                Scene scene = new Scene(logparent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setTitle("Recept!");
                app_stage.setScene(scene);
                app_stage.show();
                FXMLDocumentController torecept = Loader.getController();
                addMore();
                JSON = BookInfoObj.toString();
                torecept.Recept(JSON);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Stock Error");
                alert.setContentText("You Have This Book Out Of Stock\nYou Have " + getStock(ISBNCode) + " Your Require ment" + quantity);
                alert.showAndWait();
            }
        } catch (JSONException ex) {
            Logger.getLogger(SellBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void MoreAction(ActionEvent event) throws JSONException {
        addMore();
    }

    public void addMore() {
        try {
            quantity = Integer.parseInt(quantityField.getText());
            ISBNCodeField.setText("");
            quantityField.setText("1");
            namelabel.setText("");
            costLabel.setText("");
            jsonObj = jsonObj.put(ISBNCode, quantity);
            BookInfoObj.put("BookInfo", jsonObj);
        } catch (JSONException ex) {
            Logger.getLogger(SellBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean checkQuantity(int quantity, String ISBN) {
        int stock = 0;
        try {
            ResultSet rs = stmt.executeQuery("SELECT * FROM `books` WHERE code='" + ISBN + "'");
            while (rs.next()) {
                stock = rs.getInt("stock");
            }
        } catch (SQLException ex) {
            Logger.getLogger(SellBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (stock >= quantity) {
            return true;
        } else {
            return false;
        }
    }

    public int getStock(String ISBN) {
        int stock = 0;
        try {
            ResultSet rs = stmt.executeQuery("SELECT stock FROM `books` WHERE code='" + ISBN + "'");
            while (rs.next()) {
                stock = rs.getInt("stock");
            }
        } catch (SQLException ex) {
            Logger.getLogger(SellBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stock;
    }

    public boolean checkISBNInDb(String ISBNCode) throws SQLException {
        ResultSet rs = stmt.executeQuery("SELECT code FROM `books` WHERE code = " + ISBNCode);
        if (!rs.next()) {
            return false;
        } else {
            return true;
        }
    }
}
