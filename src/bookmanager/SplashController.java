/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class SplashController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    ProgressBar progressBar;
    @FXML
    AnchorPane ap;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        new Thread() {
            public void run() {
                try {
                    for (int i = 0; i < 50; i++) {
                        Thread.sleep(50);
                        System.out.println(i / 100.0);
                        progressBar.setProgress(i / 100.0);
                    }
                    Thread.sleep(50);
                    conn connection = new conn();
                    Statement stmt = connection.CreateStatement();
                    for (int i = 50; i < 100; i++) {
                        Thread.sleep(50);
                        System.out.println(i / 100.0);
                        progressBar.setProgress(i / 100.0);
                    }
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            changestage();
                        }
                    });
                } catch (InterruptedException ex) {
                    Logger.getLogger(SplashController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();

    }

    public void changestage() {
        FXMLLoader Loader = new FXMLLoader();

        Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
        try {
            Loader.load();
        } catch (LoadException ex) {
        } catch (IOException ex) {
        }
        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = new Stage();
        app_stage.setTitle("Admin!");
        app_stage.setScene(scene);
        app_stage.show();
        ap.getScene().getWindow().hide();
    }
}
