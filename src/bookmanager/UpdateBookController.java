package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UpdateBookController implements Initializable {

    @FXML
    Label ISBNLabel, errLabel;
    @FXML
    TextField booknamefield, authorfield, publisherfield, costfield, addMore;
    @FXML
    Label stockfield;
    @FXML
    ComboBox category;
    conn connection = new conn();
    Statement stmt = connection.CreateStatement();
    int moreInStock;
    int More = 0;

    public void setISBN(String ISBNCode) {
        try {
            ResultSet crs = stmt.executeQuery("SELECT `categorys`.* FROM `categorys`");
            while (crs.next()) {
                category.getItems().add(crs.getString("category"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UpdateBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ISBNLabel.setText(ISBNCode);
            String sql = "SELECT `books`.*, `categorys`.`category` as `cate` FROM `books` , `categorys` WHERE `books`.`code`='" + ISBNCode + "' AND `books`.`category` = `categorys`.`id` ";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String bookname = rs.getString("title");
                booknamefield.setText(rs.getString("title"));
                authorfield.setText(rs.getString("author"));
                publisherfield.setText(rs.getString("publisher"));
                costfield.setText(rs.getString("cost"));
                stockfield.setText(rs.getString("stock"));
                category.setValue(rs.getString("cate"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(UpdateBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void SubmitAction(ActionEvent event) {
        try {
            if (!costfield.equals("") || !"".equals(booknamefield) || !"".equals(authorfield) || !"".equals(publisherfield) || !"".equals(stockfield)) {
                int stock = Integer.parseInt(stockfield.getText());
                if (!isint(addMore.getText())) {
                    errLabel.setText("Please Put A Number In Stock");
                    if ("".equals(addMore.getText())) {
                        errLabel.setText("Please Put 0 Or A Number In Stock");
                    }
                } else {
                    More = Integer.parseInt(addMore.getText());
                }
                moreInStock = stock + More;
                int cost = Integer.parseInt(costfield.getText());
                String updateSql = "UPDATE `books` SET `title`='" + booknamefield.getText() + "',`author`='" + authorfield.getText() + "',`publisher`='" + publisherfield.getText() + "',`stock`=" + moreInStock + ",`code`='" + ISBNLabel.getText() + "',`cost`=" + cost + " WHERE code ='" + ISBNLabel.getText() + "'";
                stmt.execute(updateSql);
                FXMLLoader Loader = new FXMLLoader();
                Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                try {
                    Loader.load();
                } catch (LoadException ex) {
                    System.out.println(ex);
                } catch (IOException ex) {
                }

                Parent logparent = Loader.getRoot();
                Scene scene = new Scene(logparent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setTitle("Admin!");
                app_stage.setScene(scene);
                app_stage.show();
            } else {
                errLabel.setText("No Field Should Be Empty");
            }
        } catch (SQLException ex) {
            Logger.getLogger(UpdateBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isint(String num) {
        try {
            int i = Integer.parseInt(num);
            return true;
        } catch (NumberFormatException nume) {
            return false;
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
