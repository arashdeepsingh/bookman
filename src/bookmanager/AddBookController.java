package bookmanager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import SQLConn.conn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class AddBookController implements Initializable {

    @FXML
    TextField codetext, booknamefield, authorfield, publisherfield, costfield, stockfield;
    @FXML
    ComboBox category;
    @FXML
    Label errLabel;
    String ISBNCode;
    JSONObject newjson;
    String bookname;
    String author;
    String publisher;
    String json;
    conn connection = new conn();
    Statement stmt = connection.CreateStatement();

    @FXML
    private void clearAction(ActionEvent event) {
        codetext.setText(null);
        booknamefield.setText(null);
        authorfield.setText(null);
        publisherfield.setText(null);
        costfield.setText(null);
        stockfield.setText(null);
    }

    @FXML
    private void SubmitAction(ActionEvent event) {
        if (!codetext.equals("") || !booknamefield.equals("") || !authorfield.equals("") || !publisherfield.equals("") || !costfield.equals("") || !stockfield.equals("") || !category.equals(null)) {
            try {
                String category = this.category.getValue().toString();
                int categoryId = 0;
                ResultSet rs = stmt.executeQuery("SELECT * FROM `categorys` WHERE `category`='" + category + "'");
                while (rs.next()) {
                    categoryId = rs.getInt("id");
                }
                int Stock = Integer.parseInt(stockfield.getText());
                int Cost = Integer.parseInt(costfield.getText());
                String sql = "INSERT INTO `books`(`title`, `author`, `publisher`, `stock`, `code`, `cost`,`category`) VALUES ('" + bookname + "','" + author + "','" + publisher + "'," + Stock + ",'" + ISBNCode + "'," + Cost + "," + categoryId + ")";
                System.out.println(sql);
                stmt.execute(sql);
                FXMLLoader Loader = new FXMLLoader();
                Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                try {
                    Loader.load();
                } catch (LoadException ex) {
                } catch (IOException ex) {
                }
                Parent logparent = Loader.getRoot();
                Scene scene = new Scene(logparent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setTitle("Admin!");
                app_stage.setScene(scene);
                app_stage.show();
            } catch (SQLException ex) {
                Logger.getLogger(AddBookController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            errLabel.setText("Please Fill All Values");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            String sql = "SELECT * FROM `categorys`";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                category.getItems().add(rs.getString("category"));
            }
            codetext.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if (event.getCode() == KeyCode.ENTER) {
                        ISBNCode = codetext.getText();
                        URL url;
                        try {
                            if (checkISBNInDb(ISBNCode)) {
                                url = new URL("https://www.googleapis.com/books/v1/volumes?q=isbn:" + ISBNCode);
                                BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                StringBuilder sb = new StringBuilder();
                                String line = reader.readLine();
                                while (line != null) {
                                    sb.append(line);
                                    line = reader.readLine();
                                }
                                json = sb.toString();
                                JSONObject jsonObj = new JSONObject(json);
                                Iterator<String> keys = jsonObj.keys();
                                int tt = jsonObj.getInt(keys.next());
                                String ttt = jsonObj.getString(keys.next());
                                JSONArray jsonnew = jsonObj.getJSONArray("items");
                                newjson = (JSONObject) jsonnew.get(0);
                                JSONObject volinfoobj = newjson.getJSONObject("volumeInfo");
                                bookname = volinfoobj.getString("title");
                                booknamefield.setText(bookname);
                                JSONArray jsonarr = volinfoobj.getJSONArray("authors");
                                author = jsonarr.get(0).toString();
                                authorfield.setText(author);
                                publisher = volinfoobj.getString("publisher");
                                publisherfield.setText(publisher);
                                reader.close();
                            } else {
                                FXMLLoader Loader = new FXMLLoader();
                                Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                                try {
                                    Loader.load();
                                } catch (LoadException ex) {
                                    System.out.println(ex);
                                } catch (IOException ex) {
                                }

                                Parent logparent = Loader.getRoot();
                                Scene scene = new Scene(logparent);
                                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                app_stage.setTitle("Admin!");
                                app_stage.setScene(scene);
                                app_stage.show();
                                FXMLDocumentController todocument = Loader.getController();
                                todocument.EditBook(ISBNCode);

                            }
                        } catch (MalformedURLException ex) {
                        } catch (IOException ex) {
                        } catch (JSONException ex) {
                        } catch (SQLException ex) {
                            Logger.getLogger(AddBookController.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

        } catch (SQLException ex) {
            Logger.getLogger(AddBookController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean checkISBNInDb(String ISBNCode) throws SQLException {
        Statement chstmt = connection.CreateStatement();
        ResultSet rs = chstmt.executeQuery("SELECT code FROM `books` WHERE code = " + ISBNCode);
        if (!rs.next()) {
            return true;
        } else {
            return false;
        }
    }
}
