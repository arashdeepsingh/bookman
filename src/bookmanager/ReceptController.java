package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javax.jnlp.PrintService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReceptController implements Initializable {

    @FXML
    TextArea textArea;
    conn connection = new conn();
    Statement stmt = connection.CreateStatement();
    String ISBNCode;
    int quantity;
    String name;
    String cost;
    int stock;
    JSONObject SellInfo = new JSONObject();
    JSONObject jsonInfoObj = new JSONObject();
    ResultSet rs;
    JSONObject ReceptInfo = new JSONObject();
    JSONObject Info = new JSONObject();
    String Json;
    DateFormat dformat = new SimpleDateFormat("dd/mm/yyyy");
    Date date = new Date();
    String Date = dformat.format(date);
    int total;
    StringBuilder MiddleBuilder = new StringBuilder();
    String Middle;
    String Bill;

    public void getInfo(String Json) {
        textArea.setEditable(false);
        this.Json = Json;
        try {
            JSONObject jsonObj = new JSONObject(Json);
            JSONObject BookInfoObj = jsonObj.getJSONObject("BookInfo");
            Iterator keys = BookInfoObj.keys();
            while (keys.hasNext()) {
                JSONObject ISBNObj = new JSONObject();
                ISBNCode = (String) keys.next();
                quantity = BookInfoObj.getInt(ISBNCode);
                String sql = "SELECT * FROM `books` WHERE code='" + ISBNCode + "'";
                rs = stmt.executeQuery(sql);
                rs.next();
                String name = rs.getString("title");
                String cost = rs.getString("cost");
                int stock = rs.getInt("stock");
                ISBNObj.accumulate("cost", cost);
                ISBNObj.accumulate("stock", stock);
                ISBNObj.accumulate("name", name);
                ISBNObj.accumulate("quantity", quantity);
                jsonInfoObj.append(ISBNCode, ISBNObj);
            }
            SellInfo.put("SellInfo", jsonInfoObj);
            ReceptInfo = SellInfo.getJSONObject("SellInfo");
            Iterator receptKeys = ReceptInfo.keys();
            while (receptKeys.hasNext()) {
                String BookCode = (String) receptKeys.next();
                JSONArray BookInfos = ReceptInfo.getJSONArray(BookCode);
                JSONObject jobj = BookInfos.getJSONObject(0);
                String BookName = jobj.getString("name");
                int BookCost = Integer.parseInt(jobj.getString("cost"));
                int BookQuantity = jobj.getInt("quantity");
                int BookStock = jobj.getInt("stock");
                if (BookName.length() > 15) {
                    BookName = BookName.substring(0, 10) + ".";
                }
                int Cost = BookQuantity * BookCost;
                MiddleBuilder.append(BookName + "    " + BookQuantity + "    " + BookCost + "       " + Cost + "\n");
                total += Cost;
            }
            String Header = "     ****Buy Book****      \n"
                    + "Date: " + Date + "    \n"
                    + "----------------------------------\n"
                    + "Book           Qtn    Rate     Amt\n"
                    + "----------------------------------\n";
            String Footer = "----------------------------------\n"
                    + "                       Total: " + total
                    + "\n----------------------------------\n";
            Bill = Header + MiddleBuilder + Footer;
            textArea.setText(Bill);
            printCard(Bill);
        } catch (SQLException sqle) {

        } catch (JSONException ex) {
            Logger.getLogger(ReceptController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void processAction(ActionEvent event) {
        try {
            ReceptInfo = SellInfo.getJSONObject("SellInfo");
            Iterator receptKeys = ReceptInfo.keys();
            while (receptKeys.hasNext()) {
                String BookCode = (String) receptKeys.next();
                JSONArray BookInfos = ReceptInfo.getJSONArray(BookCode);
                JSONObject jobj = BookInfos.getJSONObject(0);
                String BookName = jobj.getString("name");
                int BookCost = Integer.parseInt(jobj.getString("cost"));
                int BookQuantity = jobj.getInt("quantity");
                int BookStock = jobj.getInt("stock");
                int NowInStock = BookStock - BookQuantity;
                String SQL = "UPDATE `books` SET stock=" + NowInStock + " WHERE code='" + BookCode + "'";
                stmt.execute(SQL);
            }
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
            } catch (IOException ex) {
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin!");
            app_stage.setScene(scene);
            app_stage.show();
        } catch (JSONException ex) {
            Logger.getLogger(ReceptController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReceptController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printCard(String bill) {

//        DocFlavor df = DocFlavor.INPUT_STREAM.AUTOSENSE;
//        Doc d = new SimpleDoc(bill, null, null);
//        PrintService P = PrintServiceLookup.lookupDefaultPrintService();
//
//        if (P != null) {
//            DocPrintJob job = P.createPrintJob();
//            try {
//                job.print(d, null);
//            } catch (PrintException ex) {
//                Logger.getLogger(ReceptController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream("COM1");
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ReceptController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        PrintStream ps = new PrintStream(fos);
//        ps.print(bill);
//        ps.print("\f");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

}
