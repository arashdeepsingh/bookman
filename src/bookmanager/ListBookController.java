/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class ListBookController implements Initializable {

    @FXML
    TableView<BookBin> bookt;
    @FXML
    TableColumn<BookBin, String> isbnt, titlet, publishert, authort, categoryt;
    @FXML
    TableColumn<BookBin, Integer> stockt;
    @FXML
    TextField SearchText;
    conn connection = new conn();
    Statement stmt = connection.CreateStatement();
    String sql;
    ResultSet rs;

    public void BookView() {
        sql = "SELECT `B`.*, `C`.`category` as `cate` FROM `books` as `B`,`categorys` as `C` WHERE `B`.`category`= `C`.`id`";
        try {
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String ISBN = rs.getString("code");
                String title = rs.getString("title");
                String author = rs.getString("author");
                String publisher = rs.getString("publisher");
                String category = rs.getString("cate");
                int stock = rs.getInt("stock");
                bookt.getItems().add(new BookBin(ISBN, title, publisher, author, category, stock));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListBookController.class.getName()).log(Level.SEVERE, null, ex);
        }

        SearchText.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                for (int i = 0; i < bookt.getItems().size(); i++) {
                    bookt.getItems().clear();
                }
                try {
                    if (!SearchText.getText().equals("")) {
                        String SearchQuery = SearchText.getText();
                        sql = "SELECT `B`.*, `C`.`category` as `cate` FROM `books` as `B`,`categorys` as `C` WHERE `B`.`category`= `C`.`id` AND `B`.`title` LIKE '%" + SearchQuery + "%'";
                    }
                    if (SearchText.getText().equals("")) {
                        String SearchQuery = SearchText.getText();
                        sql = "SELECT `B`.*, `C`.`category` as `cate` FROM `books` as `B`,`categorys` as `C` WHERE `B`.`category`= `C`.`id`";
                    }
                    rs = stmt.executeQuery(sql);
                    while (rs.next()) {
                        String ISBN = rs.getString("code");
                        String title = rs.getString("title");
                        String author = rs.getString("author");
                        String publisher = rs.getString("publisher");
                        String category = rs.getString("cate");
                        int stock = rs.getInt("stock");
                        bookt.getItems().add(new BookBin(ISBN, title, publisher, author, category, stock));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ListBookController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        isbnt.setCellValueFactory(new PropertyValueFactory<>("ISBN"));
        titlet.setCellValueFactory(new PropertyValueFactory<>("Title"));
        publishert.setCellValueFactory(new PropertyValueFactory<>("Publisher"));
        authort.setCellValueFactory(new PropertyValueFactory<>("Author"));
        categoryt.setCellValueFactory(new PropertyValueFactory<>("Category"));
        stockt.setCellValueFactory(new PropertyValueFactory<>("Stock"));
    }

    public void BookView(int Category) {
        sql = "SELECT `B`.*, `C`.`category` as `cate` FROM `books` as `B`,`categorys` as `C` WHERE `B`.`category`= `C`.`id` AND `B`.`category`=" + Category;
        try {
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String ISBN = rs.getString("code");
                String title = rs.getString("title");
                String author = rs.getString("author");
                String publisher = rs.getString("publisher");
                String category = rs.getString("cate");
                int stock = rs.getInt("stock");
                bookt.getItems().add(new BookBin(ISBN, title, publisher, author, category, stock));
            }
            SearchText.setOnKeyReleased(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    for (int i = 0; i < bookt.getItems().size(); i++) {
                        bookt.getItems().clear();
                    }
                    try {
                        if (!SearchText.getText().equals("")) {
                            String SearchQuery = SearchText.getText();
                            sql = "SELECT `B`.*, `C`.`category` as `cate` FROM `books` as `B`,`categorys` as `C` WHERE `B`.`category`= `C`.`id` AND `B`.`category`=" + Category + " AND `B`.`title` LIKE '%" + SearchQuery + "%'";
                        }
                        if (SearchText.getText().equals("")) {
                            String SearchQuery = SearchText.getText();
                            sql = "SELECT `B`.*, `C`.`category` as `cate` FROM `books` as `B`,`categorys` as `C` WHERE `B`.`category`= `C`.`id` AND `B`.`category`=" + Category;
                        }
                        rs = stmt.executeQuery(sql);
                        while (rs.next()) {
                            String ISBN = rs.getString("code");
                            String title = rs.getString("title");
                            String author = rs.getString("author");
                            String publisher = rs.getString("publisher");
                            String category = rs.getString("cate");
                            int stock = rs.getInt("stock");
                            bookt.getItems().add(new BookBin(ISBN, title, publisher, author, category, stock));
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(ListBookController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            isbnt.setCellValueFactory(new PropertyValueFactory<>("ISBN"));
            titlet.setCellValueFactory(new PropertyValueFactory<>("Title"));
            publishert.setCellValueFactory(new PropertyValueFactory<>("Publisher"));
            authort.setCellValueFactory(new PropertyValueFactory<>("Author"));
            categoryt.setCellValueFactory(new PropertyValueFactory<>("Category"));
            stockt.setCellValueFactory(new PropertyValueFactory<>("Stock"));
        } catch (SQLException ex) {
            Logger.getLogger(ListBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bookt.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    String ISBN = bookt.getSelectionModel().getSelectedItem().getISBN();
                    if (ISBN.equals(null) || ISBN.equals("")) {
                    } else {
                        FXMLLoader Loader = new FXMLLoader();
                        Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                        try {
                            Loader.load();
                        } catch (LoadException ex) {
                            System.out.println(ex);
                        } catch (IOException ex) {
                        }

                        Parent logparent = Loader.getRoot();
                        Scene scene = new Scene(logparent);
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        app_stage.setTitle("Admin!");
                        app_stage.setScene(scene);
                        app_stage.show();
                        FXMLDocumentController todocument = Loader.getController();
                        todocument.EditBook(ISBN);
                    }
                }
            }
        });
    }
}
