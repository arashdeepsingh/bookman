/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class AddCetagoryController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    TextField cetagoryText;
    conn connection = new conn();
    Statement stmt = connection.CreateStatement();
    String cetagory;

    @FXML
    private void AddAction(ActionEvent event) {
        try {
            cetagory = cetagoryText.getText();
            stmt.execute("INSERT INTO `categorys`(`category`) VALUES ('" + cetagory + "')");
            cetagoryText.setText(null);
        } catch (SQLException ex) {
            Logger.getLogger(AddCetagoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cetagoryText.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    try {
                        cetagory = cetagoryText.getText();
                        stmt.execute("INSERT INTO `categorys`(`category`) VALUES ('" + cetagory + "')");
                        cetagoryText.setText(null);
                    } catch (SQLException ex) {
                        Logger.getLogger(AddCetagoryController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

}
