/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookmanager;

import SQLConn.conn;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class BookCetagoryController implements Initializable {

    @FXML
    TableView<CategoryBin> categoryt;
    @FXML
    TableColumn<CategoryBin, Integer> idt;
    @FXML
    TableColumn<CategoryBin, String> category;
    conn connection = new conn();
    Statement stmt = connection.CreateStatement();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            String sql = "SELECT * FROM `categorys`";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                String category = rs.getString("category");
                categoryt.getItems().add(new CategoryBin(id, category));
            }
            categoryt.setOnMouseReleased(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getClickCount() == 2) {
                        int category = categoryt.getSelectionModel().getSelectedItem().getId();
                        if (category == 0) {
                        } else {
                            FXMLLoader Loader = new FXMLLoader();
                            Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                            try {
                                Loader.load();
                            } catch (LoadException ex) {
                            } catch (IOException ex) {
                            }
                            Parent logparent = Loader.getRoot();
                            Scene scene = new Scene(logparent);
                            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                            app_stage.setTitle("Sell!");
                            app_stage.setScene(scene);
                            app_stage.show();
                            FXMLDocumentController ToList = Loader.getController();
                            ToList.ListBookCetagory(category);
                        }
                    }
                }
            });
            idt.setCellValueFactory(new PropertyValueFactory<>("id"));
            category.setCellValueFactory(new PropertyValueFactory<>("category"));
        } catch (SQLException ex) {
            Logger.getLogger(BookCetagoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
